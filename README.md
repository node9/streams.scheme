
UI scheme for streams
---------------------

Streams is public domain communications server - ActivityPub and Nomad (the underground fediverse)

https://codeberg.org/streams/streams


- Place .php and .css files in /view/theme/redbasic/schema/
- Set node9-dark scheme in channel Settings / Display settings


