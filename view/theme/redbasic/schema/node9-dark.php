<?php
	
	if (! $nav_bg)
		$nav_bg = "#000";
	if (! $nav_gradient_top)
		$nav_gradient_top = "#000";
	if (! $nav_gradient_bottom)
		$nav_gradient_bottom = "#000";
	if (! $nav_active_gradient_top)
		$nav_active_gradient_top = "#333";
	if (! $nav_active_gradient_bottom)
		$nav_active_gradient_bottom = "#111";
	if (! $nav_bd)
		$nav_bd = "#111";
	if (! $nav_icon_colour)
		$nav_icon_colour = "#999";
	if (! $nav_active_icon_colour)
		$nav_active_icon_colour = "#fff";
	if (! $link_colour)
		$link_colour = "#aee";
	if (! $banner_colour)
		$banner_colour = "#999";
	if (! $bgcolour)
		$bgcolour = "#0b0b0b";
	if (! $item_colour)
		$item_colour = "rgba(48,48,48,0.8)";
	if (! $comment_item_colour)
		$comment_item_colour = "rgba(48,48,48,0.8)";
	if (! $comment_border_colour)
		$comment_border_colour = "rgba(28,28,28,0.8)";
	if (! $toolicon_colour)
		$toolicon_colour = '#999';
	if (! $toolicon_activecolour)
		$toolicon_activecolour = '#fff';
	if (! $font_colour)
		$font_colour = "#ccc";
	if (! $converse_width)
		$converse_width = "1024";
	if (! $radius)
		$radius = "8px";
		


